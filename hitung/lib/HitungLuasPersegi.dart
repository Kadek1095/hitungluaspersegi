import 'package:flutter/material.dart';

class HitungLuasPersegi extends StatefulWidget {
  @override
  _HitungLuasPersegiState createState() => _HitungLuasPersegiState();
}

class _HitungLuasPersegiState extends State<HitungLuasPersegi> {
  int panjang = 0;
  int lebar = 0;
  int luas = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.blue[900],
        centerTitle: true,
        title: Text('MENGHITUNG ...'),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20, right: 20, top: 40),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      panjang = int.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    suffix: Text('CM'),
                    filled: true,
                    hintText: 'Panjang',
                  ),
                )),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      lebar = int.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    suffix: Text('CM'),
                    filled: true,
                    hintText: 'Lebar',
                  ),
                )),
              ],
            ),
          ),
          Container(
            // padding: EdgeInsets.only(left: 50, top: 5),
            margin: EdgeInsets.only(top: 15, bottom: 15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                color: Colors.blue[600],
              ),
            ),

            // color: Colors.black,
            child: RaisedButton(
              padding:
                  EdgeInsets.only(left: 50, top: 10, bottom: 10, right: 50),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              color: Colors.black,
              onPressed: () {
                setState(() {
                  luas = panjang * lebar;
                });
              },
              child: Text("Hitung Luas",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500)),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 50),
              child: Column(
                children: <Widget>[
                  Text('Luas Persegi:',
                      style: TextStyle(
                        fontSize: 30,
                        color: Colors.blue[800],
                        fontWeight: FontWeight.bold,
                      )),
                  Text('$luas',
                      style: TextStyle(
                        fontSize: 60,
                        // color: Colors.green,
                      ))
                ],
              )
              // Text("Luas Persegi :"),
              // TEXT(),
              )
        ],
      ),
    );
  }
}
